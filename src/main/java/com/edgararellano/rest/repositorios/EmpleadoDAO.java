package com.edgararellano.rest.repositorios;

import com.edgararellano.rest.empleados.Capacitacion;
import com.edgararellano.rest.empleados.Empleados;
import com.edgararellano.rest.empleados.Empleado;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Repository
public class EmpleadoDAO {
    final static Logger logger =  LoggerFactory.getLogger(EmpleadoDAO.class);
    private static Empleados list = new Empleados();
    static {
        Capacitacion cap1  = new Capacitacion("2020/01/01","DBA");
        Capacitacion cap2  = new Capacitacion("2019/01/01","BACK END");
        Capacitacion cap3  = new Capacitacion("2018/01/01","FRONT END");

        ArrayList<Capacitacion> una = new ArrayList<Capacitacion>();
        una.add(cap1);
        ArrayList<Capacitacion> dos = new ArrayList<Capacitacion>();
        dos.add(cap1);
        dos.add(cap2);
        ArrayList<Capacitacion> todas = new ArrayList<Capacitacion>();
        todas.add(cap3);
        todas.addAll(dos);



        list.getListaEmpleados().add(new Empleado(1,"Antonio","Lopez","antonio@lopez.com",una));
        list.getListaEmpleados().add(new Empleado(2,"Miguel","Rodriguez","miguel@rodriguez.com",dos));
        list.getListaEmpleados().add(new Empleado(3,"Antonio","Lopez","antonio@lopez.com",todas));

    }

    public Empleados getAllEmpleados(){
        logger.debug("Aqui");
        return list;
    }

    public Empleado getEmpleado(int id){
        for (Empleado emp : list.getListaEmpleados()){
            if(emp.getId() == id){
                return emp;
            }
        }
        return null;
    }

    public void addEmpleado(Empleado emp){
        list.getListaEmpleados().add(emp);

    }

    public void updEmpleado(Empleado emp){
        Empleado current = getEmpleado(emp.getId());
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }

    public void updEmpleado(int id,Empleado emp){
        Empleado current = getEmpleado(id);
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    public String deleteEmpleado(int id){
        Empleado current = getEmpleado(id);
        if (current == null) return "Null";
        Iterator it = list.getListaEmpleados().iterator();
        while (it.hasNext()){
            Empleado emp = (Empleado) it.next();
            if (emp.getId() == id){
                it.remove();
                break;
            }
        }
        return "OK";
    }

    public void softupdEmpleado(int id, Map<String,Object> updates){
        Empleado current = getEmpleado(id);
        for(Map.Entry<String, Object> entry : updates.entrySet()){
            switch (entry.getKey()){
                case "nombre":
                    current.setNombre(entry.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido(entry.getValue().toString());
                    break;
                case "email":
                    current.setEmail(entry.getValue().toString());
                    break;

            }
        }
    }

    public List<Capacitacion> getCapacitaciones(int id){
        Empleado current = getEmpleado(id);
        ArrayList<Capacitacion> caps = new ArrayList<>();
        if (current != null) caps = current.getCapacitaciones();
        return caps;
    }

    public Boolean addCapacitacion(int id, Capacitacion cap){
        Empleado current = getEmpleado(id);
        if(current == null) return false;
        current.getCapacitaciones().add(cap);
        return true;
    }

}
