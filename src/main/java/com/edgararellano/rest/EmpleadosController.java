package com.edgararellano.rest;


import com.edgararellano.rest.empleados.Capacitacion;
import com.edgararellano.rest.empleados.Empleado;
import com.edgararellano.rest.empleados.Empleados;
import com.edgararellano.rest.repositorios.EmpleadoDAO;

import com.edgararellano.rest.utils.BadSeparator;
import com.edgararellano.rest.utils.Configuracion;
import com.edgararellano.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {
    @Autowired
    private EmpleadoDAO empDao;

    @GetMapping(path = "/")
    public Empleados getEmpleados(){
        return empDao.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        Empleado emp = empDao.getEmpleado(id);
        if (emp == null){
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(emp);
        }

    }
    @PostMapping(path = "/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){
        Integer id = empDao.getAllEmpleados().size()+1;
        emp.setId(id);
        empDao.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/",consumes = "application/json",produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp){
        empDao.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}",consumes = "application/json",produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp){
        empDao.updEmpleado(id,emp);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleado(@PathVariable int id){
        String s = empDao.deleteEmpleado(id);
        if (s == "OK"){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/{id}",consumes = "application/json",produces = "application/json")
    public ResponseEntity<Object> patchEmpleado(@PathVariable int id,@RequestBody Map<String, Object> updates){
        empDao.softupdEmpleado(id,updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "{id}/capacitaciones/")
    public  ResponseEntity<Object>  capacitacionesEmpleado(@PathVariable int id){
        List<Capacitacion> capacitaciones = empDao.getCapacitaciones(id);
        return ResponseEntity.ok().body(capacitaciones);
    }

    @PostMapping(path = "/{id}/capacitaciones",consumes = "application/json",produces = "application/json")
    public ResponseEntity<Object> addCapacitacion(@PathVariable int id,@RequestBody Capacitacion cap){
        Boolean correct = empDao.addCapacitacion(id,cap);
        if (correct) return ResponseEntity.ok().build();
        else return ResponseEntity.notFound().build();
    }
    @Value("${app.titulo}") private String titulo;

    @GetMapping("/titulo")

    public String getAppTitulo(){
        String modo = configuration.getModo();
        return String.format("%s %s",titulo,modo);
    }

    @Autowired
    private Environment env;
    @GetMapping("/autor")
    public String getAppAutor(){
        //return env.getProperty("app.autor") ;
        try {
            return configuration.getAutor();
        }catch(Exception ex){
            return " ";
        }
        }

    @Autowired
    Configuracion configuration;



    @GetMapping("/Cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String Separador){
        try {
            return Utilidades.getCadena(texto,Separador);
        } catch (BadSeparator badSeparator) {
            return badSeparator.getMessage();

        }
    }

}
