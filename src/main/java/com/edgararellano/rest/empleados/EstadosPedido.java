package com.edgararellano.rest.empleados;

public enum EstadosPedido {
    ACEPTADO,
    COCINANDO,
    EN_ENTREGA,
    ENTREGADO,
    VALORANDO
}
