package com.edgararellano.rest;


import com.edgararellano.rest.empleados.EstadosPedido;
import com.edgararellano.rest.utils.BadSeparator;
import com.edgararellano.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;


    @Test
    public void testGetCadena(){
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto,empleadosController.getCadena(origen,"."));
    }

    @Test
    public void testSeparator(){
        try {
            Utilidades.getCadena("Edgar Arellano","..");
            fail("Se esperaba BadSeparator");
        } catch (BadSeparator bs){}
    }

    @Test
    public void testGetAuthor(){
        assertEquals("Edgar Arellano",empleadosController.getAppAutor());
    }

    @ParameterizedTest
    @ValueSource(ints = {1,3,5,15,-3,Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {""," "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EmptySource
    public void testEstaBlanco2(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }


    @ParameterizedTest
    @NullAndEmptySource
    @EmptySource
    @ValueSource(strings = {"","\t","\n"})
    public void testEstaBlanco3(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }

}
